## oriole-user 12 SQ1D.220205.003 8069835 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: oriole
- Brand: google
- Flavor: oriole-user
- Release Version: 12
- Id: SQ1D.220205.003
- Incremental: 8069835
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/oriole/oriole:12/SQ1D.220205.003/8069835:user/release-keys
- OTA version: 
- Branch: oriole-user-12-SQ1D.220205.003-8069835-release-keys-random-text-5264350614345
- Repo: google_oriole_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
